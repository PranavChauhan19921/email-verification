from flask import Flask
from bson.objectid import ObjectId
from flask import jsonify, request
import pymongo
from datetime import *
app = Flask(__name__)
import re


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["harry"]
mycol = mydb["text"]


# @app.route('/verify', methods=['POST'])
# def verify():
   
#     if request.method == "POST":
  
#         user = request.get_json('NewUser')
       
#         user_found = mycol.find_one({"NewUser": user})
   
      
  
#         if user_found:
#             result = {
#                 "string": 'Authentication Success',
#                 "status":True,
#                 }
#             return jsonify(result)
   
#         else:
#             result = {
#                 "string": 'Authentication failed',
#                 "status":False,
#                 }
#             return jsonify(result)

@app.route('/namereg', methods = ['POST'])
def namereg():
    request_data = request.get_json()
    name=request_data['name']
    time=datetime.utcnow()
    request_data['time']=time
    print(request_data)
    name_regex="[A-Za-z]{2,25}( [A-Za-z]{2,25})?"
    validation=re.fullmatch(name_regex,name)
    if validation:
        user_id=request_data['user_id']
        x=mycol.find_one({"user_id": user_id}, sort=[('time', -1)])
        # id=x['_id']
        x = mycol.insert_one(request_data)
      
        # y = query_coll.insert_one(request_data)
        return jsonify({'status':'valid'})
    else:
        return jsonify({'status':'invalid'})

@app.route('/useremail', methods = ['POST'])
def user_email():
    request_data = request.get_json()
    email=request_data['email']
    print({"email":email})
    email_regex='^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    validation=re.search(email_regex,email)
    if validation:
        user_id=request_data['user_id']
        print(request_data['user_id'])
        x=mycol.find_one({"user_id": user_id}, sort=[('time', -1)])
        id=x['_id']
        update = mycol.update_one({"_id": id},{ "$set": request_data })
        # qupdate = query_coll.update_one({"_id": id},{ "$set": request_data })
        print({"update": update})
        return jsonify({'status': 'valid'})
    else:
        return jsonify({'status': 'invalid'})


if (__name__=='__main__'):
    app.run(debug=True)